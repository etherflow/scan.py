import unittest
import re
from collections import Counter
from io import StringIO

from bs4 import BeautifulSoup

from scan import (
        WORD_RE,
        extract_words_to_counter,
        find_highest_tags_by_name,
        read_stopwords,
        process,
)


class WordReTest(unittest.TestCase):
    def test_words(self):
        for word in ["word", "pre-workout", "d'Artagnan", "won't",
                     "1password", "Caesium-137", "35-hour"]:
            self.assertIsNotNone(re.fullmatch(WORD_RE, word),
                                 msg="'{}' must match".format(word))

    def test_non_words(self):
        # This types of non words filtered on another level:
        # "42", "lala-", "-lala", "lala'"
        for non_word in ["_word_", "-", "'"]:
            self.assertIsNone(re.fullmatch(WORD_RE, non_word),
                              msg="'{}' must no match".format(non_word))


class WordsExtractingTest(unittest.TestCase):
    def test_basic(self):
        text = "One\n\ntwo\t'three' 42 hehe- -hehe hehe"
        self.assertEqual(extract_words_to_counter(text), Counter(one=1, two=1, three=1, hehe=3))


class FindTopTagsTest(unittest.TestCase):
    def test_basic(self):
        html = """<html><body>
        <div>
            pre
            <div>one</div>
        </div>
        <div>two</div>
        </body>
        </html>"""
        doc = BeautifulSoup(html, 'html.parser')
        tags = list(find_highest_tags_by_name(doc, 'div'))
        self.assertEqual(len(tags), 2)
        self.assertTrue("pre" in tags[0].text)
        self.assertTrue("one" in tags[0].text)
        self.assertEqual(tags[1].text.strip(), "two")


class ProcessTest(unittest.TestCase):
    def test_basic(self):
        html = """<html><body>
        <head><title>Title</title></head>
        <div>
            two
            <div>one</div>
            <script>alert("Hi, I'm inside div");</script>
        </div>
        <div>two <div>three four four</div><div>three</div></div>
        <div>four <strong>four</strong> three</div>
        <p>one</p>
        </body>
        </html>"""

        title, counter = process(html)
        self.assertEqual(title, "Title")
        self.assertEqual(counter, Counter(one=1, two=2, three=3, four=4))

    def test_stopwords(self):
        html = """<title>Stopwords</title>
        <div>we are the champions</div>
        """
        stopwords = """we
        are
        """
        _, counter = process(html, read_stopwords(StringIO(stopwords)))
        self.assertEqual(counter, Counter(the=1, champions=1))

    def test_html_entities(self):
        html = """<title>Entities</title><div>;o&#45o& o&#39;s</div>
        <div>o-o o's</div>
        """
        _, counter = process(html)
        self.assertEqual(counter, Counter({"o-o": 2, "o's": 2}))


if __name__ == '__main__':
    unittest.main()
