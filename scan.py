#!/usr/bin/env python3

"""

Count words in divs in html page by given url, filtered by stopwords list

"""

import argparse
import errno
import re
import sys
from collections import deque, Counter
from typing import List, Tuple, Generator, Union
from typing.io import TextIO
from urllib import request

from bs4 import BeautifulSoup
from bs4.element import Tag


WORD_RE = r"[a-zA-Z0-9]+[a-zA-Z0-9'-]*"
NON_TEXT_TAGS = ['script', 'style']


def find_highest_tags_by_name(doc: BeautifulSoup, tag_name: str) -> Generator[Tag, None, None]:
    """ Find highest tags in the tree with given tag name """
    queue = deque(doc.children)
    while queue:
        item = queue.popleft()
        if item.name == tag_name:
            yield item
        else:
            if hasattr(item, 'children'):
                queue += item.children


def extract_words_to_counter(text: str) -> Counter:
    """ Return a counter of words in given text """
    return Counter([s.lower().strip("'-") for s in re.findall(WORD_RE, text) if not s.isnumeric()])


def get_tag_text(tag: Tag) -> str:
    """ Get all text from given tag, ignoring html code and non-text tags """
    for bad_tag_name in NON_TEXT_TAGS:
        for bad_tag in tag(bad_tag_name):
            bad_tag.extract()
    return tag.get_text(separator=' ')


def process(markup: Union[str, TextIO], stopwords: List[str] = None) -> Tuple[str, Counter]:
    """ Process html and return title and counter of all words in divs """
    doc = BeautifulSoup(markup, 'html.parser')  # html5lib
    divs = find_highest_tags_by_name(doc, 'div')
    counter = sum([extract_words_to_counter(get_tag_text(div)) for div in divs], Counter())
    if stopwords:
        for stopword in stopwords:
            if stopword in counter:
                counter.pop(stopword)
    return doc.title.text, counter


def read_stopwords(fileobj: TextIO) -> List[str]:
    """ Read stopwords from file-like object """
    return [line.strip() for line in fileobj.readlines()]


def main(args: argparse.Namespace) -> int:
    errcode = 0

    try:
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0",
        }
        response = request.urlopen(request.Request(args.url, headers=headers))
    except ValueError:
        sys.stderr.write("Wrong URL: {}\n".format(args.url))
        errcode = errno.EINVAL
    except request.URLError as exc:
        sys.stderr.write("Connection problem: {}\n".format(exc.reason))
        errcode = errno.ENETUNREACH
    else:
        if response.headers.get('Content-Type').startswith('text/html'):
            with open(args.stopwords) as fileobj:
                stopwords = read_stopwords(fileobj)

            title, counter = process(response.read(), stopwords)

            longest_word = max([len(word) for word in counter.keys()])
            longest_number = max([len(str(num)) for num in counter.values()])

            print(title, '\n')
            for word, value in counter.most_common():
                print('{{:{}}} {{:{}}}'.format(longest_word, longest_number).format(word, value))
        else:
            sys.stderr.write("Expected: text/html, received: {}\n".format(
                response.headers.get('Content-Type')))
            errcode = errno.EINVAL

    return errcode


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Count words in divs by url")
    parser.add_argument('url')
    parser.add_argument('--stopwords', type=str, default='stopwords.txt',
                        help="Path to file with stopwords list")

    sys.exit(main(parser.parse_args()))
